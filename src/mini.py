import sys
from html_minifier.minify import Minifier

with open(sys.argv[1]) as f:
    html = "\n".join(f.readlines())
    minifier = Minifier(html)
    html = minifier.minify()
    print(html)